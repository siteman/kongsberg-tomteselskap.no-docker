��          \      �       �      �      �      �   O     `   U  :   �     �  4       <     O     _  X     `   �  :   9     t                                       Column Link Fernando Acosta Make Column Clickable Elementor Simple: allow users to click in the whole column instead of individual elements https://fernandoacosta.net/?utm_source=wp-org&utm_medium=site&utm_campaign=make-column-clickable https://fernandoacosta.net/make-column-clickable-elementor https://your-link.com PO-Revision-Date: 2022-06-21 18:17:57+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: GlotPress/4.0.0-alpha.1
Language: de
Project-Id-Version: Plugins - Make Column Clickable Elementor - Development (trunk)
 Individueller Link Fernando Acosta Make Column Clickable Elementor Einfach: Erlaube Benutzern, in die gesamte Spalte statt in einzelne Elemente zu klicken. https://fernandoacosta.net/?utm_source=wp-org&utm_medium=site&utm_campaign=make-column-clickable https://fernandoacosta.net/make-column-clickable-elementor https://your-link.com 