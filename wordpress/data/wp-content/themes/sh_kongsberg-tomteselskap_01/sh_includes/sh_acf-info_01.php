<?php if( have_rows('sh_informasjon') ): ?>
 
    <ul>
 
    <?php while( have_rows('sh_informasjon') ): the_row(); ?>
 
        <li>
        	<span><?php the_sub_field('sh_infotittel'); ?></span>
	        <span><?php the_sub_field('sh_infotekst'); ?></span>
        </li>
        
    <?php endwhile; ?>
 
    </ul>
 
<?php else : ?>
	
	<div class="sh_info_text">
		Mer informasjon kommer snart.
	</div>

<?php endif; ?>