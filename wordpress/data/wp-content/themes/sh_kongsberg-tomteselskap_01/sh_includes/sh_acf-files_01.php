<?php if( have_rows('dokumentasjon') ): ?>
 
    <ul>
 
    <?php while( have_rows('dokumentasjon') ): the_row(); ?>
 
        <li>
        	<a href="<?php the_sub_field('fil'); ?>">
	        	<?php the_sub_field('sh_filnavn'); ?>
				<i aria-hidden="true" class="fas fa-arrow-down"></i>
        	</a>
        </li>
        
    <?php endwhile; ?>
 
    </ul>

<?php else : ?>
	
	<div class="sh_info_text">
		Ingen dokumenter tilgjengelig.
	</div>

<?php endif; ?>