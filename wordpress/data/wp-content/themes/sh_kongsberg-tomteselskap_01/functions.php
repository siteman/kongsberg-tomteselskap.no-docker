<?php
/**
 * Theme functions and definitions
 *
 * @package HelloElementorChild
 */

/**
 * Load child theme css and optional scripts
 *
 * @return void
 */
function hello_elementor_child_enqueue_scripts() {
	wp_enqueue_style(
		'hello-elementor-child-style',
		get_stylesheet_directory_uri() . '/style.css',
		[
			'hello-elementor-theme-style',
		],
		'1.0.0'
	);
}
add_action( 'wp_enqueue_scripts', 'hello_elementor_child_enqueue_scripts' );

// Hent ut telefonnummer og fjern mellomrom Shortcode
function sh_acf_cleanphone( $atts ) {
	//Vars
	$term = get_field('sh_telefon');
	$term_clean = preg_replace("/[^0-9]/", "", $term);
	
	return $term_clean;
}
add_shortcode( 'sh_cleanphone', 'sh_acf_cleanphone');

function sh_child_theme_head_cookie_script() {
	if ( class_exists('\Elementor\Plugin') && ( \Elementor\Plugin::$instance->editor->is_edit_mode() || \Elementor\Plugin::$instance->preview->is_preview_mode() ) ) {
		return;
	}
	?>
	<script id="CookieConsent" src="https://policy.app.cookieinformation.com/uc.js" data-culture="NB" type="text/javascript" data-gcm-enabled="false"></script>
<?php }
add_action( 'wp_head', 'sh_child_theme_head_cookie_script' , 1 );

// Add GTM script to wp_head()
function sh_child_theme_head_script() { ?>
	<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-M48RVVM');</script>
<!-- End Google Tag Manager -->
<?php }
add_action( 'wp_head', 'sh_child_theme_head_script' );

// Add GTM script to wp_body_open()
function sh_child_theme_body_open_script() { ?>
	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5FHTHGX"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->
<?php }
add_action( 'wp_body_open', 'sh_child_theme_body_open_script' );




	
// Ekskludere sticky posts, forside: siste nytt
add_action( 'elementor/query/sh_homepage_news', function( $query ){
	$query->set('post__not_in', get_option('sticky_posts'));	
} );


//Include SC
function sh_template_part( $atts, $content = null ){
   $tp_atts = shortcode_atts(array( 
      'path' =>  null,
   ), $atts);         
   ob_start();  
   get_template_part($tp_atts['path']);  
   $ret = ob_get_contents();  
   ob_end_clean();  
   return $ret;    
}
add_shortcode('sh_include', 'sh_template_part');  




// Populerer alle Elementor template sections i ACF felt
function acf_load_vehicle_field_choices( $field ) {

    // reset choices
    $field['choices'] = array(
		//Må legge til Placeholder....
		'' => '',
	);

    //do the query of Vehicle Info for all the vehicles available
    $args = array(
      'numberposts'				=> -1,
      'post_type'				=> 'elementor_library',
      'elementor_library_type' 	=> 'section',
	  'order' => 'DESC',

    );
    // Do your stuff, e.g.
    $the_query = new WP_Query( $args );
    
    if ($the_query->have_posts()) {
      //global $post;
	  while( $the_query->have_posts() ){
        $the_query->the_post();
        //$value = get_post_field( 'post_name', get_the_ID() );
		//$value = '[elementor-template id="' . get_the_id() . '"]';
		$value = get_the_id();
		$label = get_the_title();
		  
        // append to choices
        $field['choices'][ $value ] = $label;
      }
      wp_reset_postdata();
    }
	
    // return the field
	return $field;

}

add_filter('acf/load_field/name=sh_tomtekart_sc', 'acf_load_vehicle_field_choices');




// Hent ut telefonnummer og fjern mellomrom Shortcode
function sh_acf_imagemap( $atts ) {
	//Vars
	$field = get_field_object( 'sh_tomtekart_sc' );
	$value = $field['value'];
	$label = $field['choices'][ $value ];
	
	return '[elementor-template id="' . $value . '"]';
}
add_shortcode( 'sh_imagemap', 'sh_acf_imagemap');






// example custom post type is "event"
// example custom taxonomy is "events-category"

function remove_default_event_category_metabox() {
	remove_meta_box( 'tagsdiv-status', 'prosjekter', 'side' );
	remove_meta_box( 'tagsdiv-marked', 'prosjekter', 'side' );
	remove_meta_box( 'tagsdiv-tomtetype', 'prosjekter', 'side' );
	remove_meta_box( 'tagsdiv-historie_arstall', 'historie', 'side' );
}
add_action( 'admin_menu' , 'remove_default_event_category_metabox' );


// Tillat HTML i utdrag
function sh_html_excerpt($text) { // Fakes an excerpt if needed
    global $post;
    if ( '' == $text ) {
        $text = get_the_content('');
        $text = apply_filters('the_content', $text);
        $text = str_replace('\]\]\>', ']]&gt;', $text);
        /*just add all the tags you want to appear in the excerpt --
        be sure there are no white spaces in the string of allowed tags */
        $text = strip_tags($text,'<p><br><b><a><em><strong>');
        /* you can also change the length of the excerpt here, if you want */
        $excerpt_length = 55; 
        $words = explode(' ', $text, $excerpt_length + 1);
        if (count($words)> $excerpt_length) {
            array_pop($words);
            array_push($words, '[...]');
            $text = implode(' ', $words);
        }
    }
    return $text;
}

/* remove the default filter */
remove_filter('get_the_excerpt', 'wp_trim_excerpt');

/* now, add your own filter */
add_filter('get_the_excerpt', 'sh_html_excerpt');
